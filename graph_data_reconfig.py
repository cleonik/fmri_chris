import scipy.io as sio
import numpy as np
import glob
import os

def make_static_into_csv(filename):
    f = open(filename, 'r')
    lines = f.readlines()
    f.close()

    for i in range(len(lines)):
        lines[i] = lines[i].replace(' ','').replace('\n','').split('\t')
        lines[i] = lines[i][:-1]

    filename = filename[:-4] + '.csv'
    print(filename)
    f = open(filename, 'w')
    for i in range(len(lines)):
        s = ','.join(lines[i])
        f.write(s)
        f.write('\n')
    f.close()

def make_dynamic_into_csv(filename):
    mat_content = sio.loadmat(filename)
    if 'z' in filename:
        index_string = 'DZStruct'
    elif 'r' in filename:
        index_string = 'DRStruct'
    else:
        print('Filename error')
        return
    data = mat_content[index_string][0][0]
    names = mat_content[index_string].dtype.names
    for i in range(len(names)):
        subj = filename[-13:-4]
        if filename[-34] == '/': #One digit number. eg: z6_A00038998.mat
            root = filename[:-33]
        else:
            root = filename[:-34]
        file = root + subj + '/' + index_string[1].lower() + names[i] + '.csv'
        if not os.path.isdir(root + subj):
            os.mkdir(root + subj)
        f = open(file, 'w')
        for row in range(len(data[i][0])):
            s = ""
            for col in range(len(data[i])):
                s += str(data[i][col][row]) + ','
            s = s[:-1] + '\n'
            f.write(s)
        f.close()
################################################################################

# filenames = ['GretnaSFCMatrixZ/zDMNTRACKINGTEST.txt',
#              'GretnaSFCMatrixR/rDMNTRACKINGTEST.txt',
#              'GretnaTimeCourse/DMNTRACKINGTEST.txt']
#
# for file in filenames:
#     make_static_into_csv(file)

#fname = 'GretnaDFCMatrixR/rDMNTRACKINGTEST.mat'
#make_dynamic_into_csv(fname)

#filenames = ['GretnaDFCMatrixR/rDMNTRACKINGTEST.mat','GretnaDFCMatrixZ/zDMNTRACKINGTEST.mat']
filenames = glob.glob('/Users/Chris/Desktop/DATA/GretnaDFCMatrixZ/z*.mat')
filenames += glob.glob('/Users/Chris/Desktop/DATA/GretnaDFCMatrixR/r*.mat')

for fname in filenames:
    print(fname)
    make_dynamic_into_csv(fname)

# mat_content['DRStruct'] stores the DFC Matrix
# Easiest to see if imported into Matlab
# First & Second dimension must be set to 0
# Third dimension is the Sliding Window Index
# Fourth dimension is the column
# Fifth dimension is the row
#print(mat_content['DRStruct'][0][0][362][0][1])
