import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import glob
import os

def print_graph(G):
    for (u, v, wt) in G.edges.data('weight'):
        print('(%d, %d, %.3f)' % (u, v, wt))

def threshold(G, t):
    matrix = nx.to_numpy_matrix(G)
    #print(matrix[0,0])
    for row in range(matrix.shape[0]):
        for col in range(matrix.shape[1]):
            if (matrix[row, col] > t):
                matrix[row, col] = 1
            else:
                matrix[row, col] = 0
    return nx.from_numpy_matrix(matrix)

def sparsity(G):
    num_nodes = nx.number_of_nodes(G)
    num_edges = nx.number_of_edges(G)
    max_edges = (num_nodes * (num_nodes - 1))/2
    return num_edges / max_edges

def find_cutoff(G, sparsity_target, tol):
    left = -1
    right = 1
    left_val = sparsity(threshold(G, left))
    right_val = sparsity(threshold(G, right))
    if abs(right_val - sparsity_target) < abs(left_val - sparsity_target):
        min = [right, right_val]
    else:
        min = [left, left_val]
    if left_val < sparsity_target:
        print('Left cutoff must be decreased')
    elif right_val > sparsity_target:
        print('Right cutoff must be increased')
    else:
        current = (left + right)/2
        curr_val = sparsity(threshold(G, current))
        if abs(curr_val - sparsity_target) < abs(min[1] - sparsity_target):
            min = [current, curr_val]
        counter = 0
        while abs(curr_val - sparsity_target) > tol and counter < 20:
            if curr_val < sparsity_target:
                right = current
                right_val = sparsity(threshold(G, right))
            else: #curr_val > sparsity_target
                left = current
                left_val = sparsity(threshold(G, left))
            current = (left + right)/2
            curr_val = sparsity(threshold(G, current))
            if abs(curr_val - sparsity_target) < abs(min[1] - sparsity_target):
                min = [current, curr_val]
            counter += 1
        #print('%.3f produces a sparsity of %.3f' % (min[0], min[1]))
        return min[0]

def DMN_indices():
    return np.append(np.append(np.append(np.arange(73,83),np.arange(85,131)),136),138)

def SN_indices():
    return np.arange(202, 220)

def get_isolated_region(G, indices):
    data = nx.to_numpy_matrix(G)
    region = data[np.ix_(indices, indices)]
    return nx.from_numpy_matrix(region)
def get_both_regions(G, indicesA, indicesB):
    data = nx.to_numpy_matrix(G)
    #print(indicesA)
    #print(indicesB)
    both = np.concatenate((indicesA, indicesB))
    region = data[np.ix_(both, both)]
    return nx.from_numpy_matrix(region)

def graph_measures(G):
    if G == None:
        return None
    avg_clust = nx.average_clustering(G)
    glob_eff = nx.global_efficiency(G)
    char_path_len = 1/glob_eff
    try:
        avg_short_path = nx.average_shortest_path_length(G)
    except nx.NetworkXError as err:
        print(str(err) + ' Setting Avg Shortest Path Len = 0')
        avg_short_path = 0
    #add nx.local_efficiency
    local_eff = nx.local_efficiency(G)
    return [avg_clust, glob_eff, char_path_len, avg_short_path, local_eff]

def write_Chris_CSV(file, measures, sparsity, threshold):
    if measures == None:
        return
    new_line = str(sparsity) + ',' + str(threshold) + ',' + str(measures[0]) + ','
    new_line += str(measures[1]) + ',' + str(measures[2]) + ',' + str(measures[3]) + ',' + str(measures[4]) + '\n'
    file.write(new_line)
    return

def extract_graph_info(file, indicesA, indicesB=np.arange(0,1), all3=True):
    print('Pulling info:\t%s...' % (file))

    data = pd.read_csv(file, header=None).values

    G = nx.from_numpy_matrix(data)

    # Gets rid of all self connections. (ie: Node 0 is connected to Node 0 with a weight of 1)
    for i in range(len(data)):
        G.remove_edge(i,i)

    Afile = file[:-16] + 'infoDMN' + file[-14:]
    Bfile = file[:-16] + 'infoSN' + file[-14:]
    ABfile = file[:-16] + 'infoBOTH' + file[-14:]

    print('Writing info:\t%s...' % (Afile))
    print('Writing info:\t%s...' % (Bfile))
    print('Writing info:\t%s...' % (ABfile))

    a = open(Afile, 'w')
    b = open(Bfile, 'w')
    ab = open(ABfile, 'w')

    a.write('Sparsity,Threshold,Avg Clustering,Global Efficiency,Char Path Len,Avg Shortest Path Len,Local Efficiency\n')
    b.write('Sparsity,Threshold,Avg Clustering,Global Efficiency,Char Path Len,Avg Shortest Path Len,Local Efficiency\n')
    ab.write('Sparsity,Threshold,Avg Clustering,Global Efficiency,Char Path Len,Avg Shortest Path Len,Local Efficiency\n')


    for s in np.arange(0.1, 0.51, 0.05): #Goes through sparsity range of 0.1 - 0.5 inclusive
        cutoff = find_cutoff(G, s, tol=1e-3)
        new_G = threshold(G, cutoff)
        print('Calc measures:\tSparsity = %.3f\tThreshold = %.3f' % (s, cutoff))
        if indicesB[0] == 0:
            regionA = get_isolated_region(new_G, indicesA) #successfully extracts DMN
            regionB = None
            regionAB = None
        else:
            if all3:
                regionA = get_isolated_region(new_G, indicesA)
                regionB = get_isolated_region(new_G, indicesB)
                regionAB = get_both_regions(new_G, indicesA, indicesB)
            else:
                regionA = None
                regionB = None
                regionAB = get_both_regions(new_G, indicesA, indicesB)
        write_Chris_CSV(a, graph_measures(regionA), s, cutoff)
        write_Chris_CSV(b, graph_measures(regionB), s, cutoff)
        write_Chris_CSV(ab, graph_measures(regionAB), s, cutoff)

    a.close()
    b.close()
    ab.close()
    print('--------------------------------------------------------------------------------')
################################################################################

filenames = glob.glob('../DATA/A*/z*')
# print(len(filenames)) 16802
for f in filenames[0:1]:
    print(f)
    extract_graph_info(f, DMN_indices(), SN_indices())
