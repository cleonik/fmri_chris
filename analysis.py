import numpy as np
import pandas as pd
import glob
import os
import csv
from scipy import stats

def get_valid_tr(root):
    #root = '/Volumes/My Passport/DATA/A00060372/'
    neurofeedback_file = root + 'neurofeedback.csv'
    electo_file = root + 'electro.csv'

    neuro_dataframe = pd.read_csv(neurofeedback_file)
    neuro_data = neuro_dataframe.values

    focus_tr = []
    wander_tr = []

    # Each row in neurofeedback csv file follows the below format:
    # [Time1, TR, Time2, Left Text, Right Text, Stim Text, Show, Sign, Classifier Output, Cumulative Score]
    for i in range(neuro_data.shape[0]):
        row = neuro_data[i,:]
        show = int(row[6])
        stim_txt = row[5]
        if show == 1:
            try:
                if stim_txt == neuro_data[i+30,5] and stim_txt == neuro_data[i+15,5]:
                    if stim_txt == 'Focus':
                        focus_tr.append(row[1])
                    else:
                        wander_tr.append(row[1])
            except IndexError:
                break
    return [focus_tr, wander_tr]

def make_info_name(x):
    x = str(x)
    end_x = str(int(x)+29)
    return 'info_' + (4-len(x))*'0' + x + '_' + (4-len(end_x))*'0' + end_x + '.csv'

################################################################################
################################################################################
################################################################################

def analyze_single(subj = 'A00060372'):
    # Get a list of valid TR for both focus & wander tasks
    #focus_tr, wander_tr = get_valid_tr('/Volumes/My Passport/DATA/A00060372/')
    focus_tr, wander_tr = get_valid_tr('/Volumes/My Passport/DATA/' + subj + '/')

    # Put the calculated graph measures into matrices
    # Each matrix is 9 x num_data x 4
    # 9 is from 9 sparsity values, 4 is from 4 graph measures
    focus_info = np.full((9,len(focus_tr),4),0.0)
    #root = '/Users/Chris/Desktop/DATA/A00060372/'
    root = '/Users/Chris/Desktop/DATA/' + subj + '/'

    for i in range(len(focus_tr)):
        file = root + make_info_name(focus_tr[i])
        info = pd.read_csv(file).values
        for j in range(9):
            focus_info[j,i,:] = info[j,2:].tolist()

    #print(focus_info)

    wander_info = np.full((9,len(wander_tr),4),0.0)

    for i in range(len(wander_tr)):
        file = root + make_info_name(wander_tr[i])
        info = pd.read_csv(file).values
        for j in range(9):
            wander_info[j,i,:] = info[j,2:].tolist()

    #print(wander_info)

    # Use Wilcoxon test for a more conservative estimate of the p-value
    # Paired t-test assumes that the data comes from a normal distribution
    spar = [0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5]
    print('Outputting p-values for difference in means between focus & wander...')
    for i in range(len(spar)):
        # print('Statistics for sparsity = %.2f:' % (spar[i]))
        # res = stats.ttest_rel(focus_info[i,:,0], wander_info[i,:,0])
        # print('\tAverage Cluster:\t' + str(res[1]))
        # res = stats.ttest_rel(focus_info[i,:,1], wander_info[i,:,1])
        # print('\tGlobal Efficiency:\t' + str(res[1]))
        # res = stats.ttest_rel(focus_info[i,:,2], wander_info[i,:,2])
        # print('\tChar Path Length:\t' + str(res[1]))
        # res = stats.ttest_rel(focus_info[i,:,3], wander_info[i,:,3])
        # print('\tAvg Short Path Length:\t' + str(res[1]))

        print('Statistics for sparsity = %.2f:' % (spar[i]))
        res = stats.wilcoxon(focus_info[i,:,0], wander_info[i,:,0])
        print('\tAverage Cluster:\t' + str(res[1]))
        res = stats.wilcoxon(focus_info[i,:,1], wander_info[i,:,1])
        print('\tGlobal Efficiency:\t' + str(res[1]))
        res = stats.wilcoxon(focus_info[i,:,2], wander_info[i,:,2])
        print('\tChar Path Length:\t' + str(res[1]))
        res = stats.wilcoxon(focus_info[i,:,3], wander_info[i,:,3])
        print('\tAvg Short Path Length:\t' + str(res[1]))

        # print('Shaprio test for sparsity = %.2f:' % (spar[i]))
        # res1 = stats.shapiro(focus_info[i,:,0])[1]
        # res2 = stats.shapiro(wander_info[i,:,0])[1]
        # print('\tAverage Cluster normality:\t' + str(res1) + ' ' + str(res2))
        # res1 = stats.shapiro(focus_info[i,:,1])[1]
        # res2 = stats.shapiro(wander_info[i,:,1])[1]
        # print('\tGlobal Efficiency normality:\t' + str(res1) + ' ' + str(res2))
        # res1 = stats.shapiro(focus_info[i,:,2])[1]
        # res2 = stats.shapiro(wander_info[i,:,2])[1]
        # print('\tChar Path Length normality:\t' + str(res1) + ' ' + str(res2))
        # res1 = stats.shapiro(focus_info[i,:,3])[1]
        # res2 = stats.shapiro(wander_info[i,:,3])[1]
        # print('\tAvg Short Path Length normality:\t' + str(res1) + ' ' + str(res2))

def get_meditation(subj):
    meditation_data = pd.read_csv('QME.csv',skiprows=1, usecols=[0,6]).values
    for i in range(meditation_data.shape[0]):
        if subj == meditation_data[i,0]:
            return meditation_data[i,1]
################################################################################
################################################################################
################################################################################
root = '/Users/Chris/Desktop/DATA/'
subj = glob.glob('/Users/Chris/Desktop/DATA/A*')
for i in range(len(subj)):
    subj[i] = subj[i].split('/')[-1] # Pulls out just subj name, eg: A00060372

# focus_count = 0
# wander_count = 0
# avg_wander_values = np.zeros((len(subj),9,5))
# avg_focus_values = np.zeros((len(subj),9,5))
# print('Pulling info from subj info files...')
# for i in range(len(subj)):
#     focus_tr, wander_tr = get_valid_tr('/Volumes/My Passport/DATA/' + subj[i] + '/')
#     focus_info  = np.zeros((9,len(focus_tr), 4))
#     wander_info = np.zeros((9,len(wander_tr),4))
#
#     for j in range(len(focus_tr)):
#         file = root + subj[i] + '/' + make_info_name(focus_tr[j])
#         info = pd.read_csv(file).values
#         for k in range(9):
#             focus_info[k,j,:] = info[k,2:].tolist()
#
#     for j in range(len(wander_tr)):
#         file = root + subj[i] + '/' + make_info_name(wander_tr[j])
#         info = pd.read_csv(file).values
#         for k in range(9):
#             wander_info[k,j,:] = info[k,2:].tolist()
#
#     for j in range(9):
#         for k in range(4):
#             avg_wander_values[i,j,k] = wander_info[j,:,k].mean()
#             avg_focus_values[i,j,k] = focus_info[j,:,k].mean()
#
#     meditation_value = get_meditation(subj[i])
#     avg_wander_values[i,:,4] = np.full((9), meditation_value)
#     avg_focus_values[i,:,4] = np.full((9), meditation_value)
#
# print(avg_wander_values.shape)
# np.save('avg_wander_values_v2', avg_wander_values)
# np.save('avg_focus_values_v2', avg_focus_values)

# focus_count = 0
# wander_count = 0
# print('Pulling info from subj info files...')
# for i in range(len(subj)):
#     focus_tr, wander_tr = get_valid_tr('/Volumes/My Passport/DATA/' + subj[i] + '/')
#     if i == 0:
#         focus_info  = np.zeros((9,len(focus_tr), 5))
#         wander_info = np.zeros((9,len(wander_tr),5))
#     else:
#         focus_info  = np.append(focus_info,  np.zeros((9,len(focus_tr), 5)), axis=1)
#         wander_info = np.append(wander_info, np.zeros((9,len(wander_tr),5)), axis=1)
#
#     for j in range(len(focus_tr)):
#         file = root + subj[i] + '/' + make_info_name(focus_tr[j])
#         info = pd.read_csv(file).values
#         for k in range(9):
#             focus_info[k,focus_count,:4] = info[k,2:].tolist()
#         focus_count += 1
#
#     for j in range(len(wander_tr)):
#         file = root + subj[i] + '/' + make_info_name(wander_tr[j])
#         info = pd.read_csv(file).values
#         for k in range(9):
#             wander_info[k,wander_count,:4] = info[k,2:].tolist()
#         wander_count += 1
#
#     meditation_value = get_meditation(subj[i])
#     for s in range(9):
#         focus_info[s,focus_count-len(focus_tr):focus_count,4] = np.full((len(focus_tr)), meditation_value)
#         wander_info[s,wander_count-len(wander_tr):wander_count,4] = np.full((len(wander_tr)), meditation_value)
#
# print(focus_info)
# print(focus_info.shape)
# np.save('all_focus_values_v2', focus_info)
# np.save('all_wander_values_v2', wander_info)

# spar = [0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5]
# print('Outputting p-values for difference in means between focus & wander...')
# for i in range(len(spar)):
#     # print('Statistics for sparsity = %.2f:' % (spar[i]))
#     # res = stats.ttest_rel(focus_info[i,:,0], wander_info[i,:,0])
#     # print('\tAverage Cluster:\t' + str(res[1]))
#     # res = stats.ttest_rel(focus_info[i,:,1], wander_info[i,:,1])
#     # print('\tGlobal Efficiency:\t' + str(res[1]))
#     # res = stats.ttest_rel(focus_info[i,:,2], wander_info[i,:,2])
#     # print('\tChar Path Length:\t' + str(res[1]))
#     # res = stats.ttest_rel(focus_info[i,:,3], wander_info[i,:,3])
#     # print('\tAvg Short Path Length:\t' + str(res[1]))
#     #
#     # print('Statistics for sparsity = %.2f:' % (spar[i]))
#     # res = stats.wilcoxon(focus_info[i,:,0], wander_info[i,:,0])
#     # print('\tAverage Cluster:\t' + str(res[1]))
#     # res = stats.wilcoxon(focus_info[i,:,1], wander_info[i,:,1])
#     # print('\tGlobal Efficiency:\t' + str(res[1]))
#     # res = stats.wilcoxon(focus_info[i,:,2], wander_info[i,:,2])
#     # print('\tChar Path Length:\t' + str(res[1]))
#     # res = stats.wilcoxon(focus_info[i,:,3], wander_info[i,:,3])
#     # print('\tAvg Short Path Length:\t' + str(res[1]))
#
#     # print('Statistics for sparsity = %.2f:' % (spar[i]))
#     # res = stats.kruskal(focus_info[i,:,0], wander_info[i,:,0])
#     # print('\tAverage Cluster:\t' + str(res[1]))
#     # res = stats.kruskal(focus_info[i,:,1], wander_info[i,:,1])
#     # print('\tGlobal Efficiency:\t' + str(res[1]))
#     # res = stats.kruskal(focus_info[i,:,2], wander_info[i,:,2])
#     # print('\tChar Path Length:\t' + str(res[1]))
#     # res = stats.kruskal(focus_info[i,:,3], wander_info[i,:,3])
#     # print('\tAvg Short Path Length:\t' + str(res[1]))
#
#     print('Shaprio test for sparsity = %.2f:' % (spar[i]))
#     res1 = stats.normaltest(focus_info[i,:,0])[1]
#     res2 = stats.normaltest(wander_info[i,:,0])[1]
#     print('\tAverage Cluster normality:\t' + str(res1) + ' ' + str(res2))
#     res1 = stats.normaltest(focus_info[i,:,1])[1]
#     res2 = stats.normaltest(wander_info[i,:,1])[1]
#     print('\tGlobal Efficiency normality:\t' + str(res1) + ' ' + str(res2))
#     res1 = stats.normaltest(focus_info[i,:,2])[1]
#     res2 = stats.normaltest(wander_info[i,:,2])[1]
#     print('\tChar Path Length normality:\t' + str(res1) + ' ' + str(res2))
#     res1 = stats.normaltest(focus_info[i,:,3])[1]
#     res2 = stats.normaltest(wander_info[i,:,3])[1]
#     print('\tAvg Short Path Length normality:\t' + str(res1) + ' ' + str(res2))
#
# # for i in range(9):
# #     print('Values for sparsity %.3f' % (spar[i]))
# #     print('\tRandom Clustering Coeff:\t%.5f' % (focus_info[i,:,0].mean()))
# #     print('\tMean Random Shortest Path:\t%.5f' % (focus_info[i,:,3].mean()))
