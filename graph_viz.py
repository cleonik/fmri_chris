import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

data = pd.read_csv('GretnaDFCMatrixZ/zW_0040_0069.csv', header=None).values
#DMN = data[[73:82,85:130,136,138],[73:82,85:130,136,138]]
DMN_node_index = np.append(np.append(np.append(np.arange(73,83),np.arange(85,131)),136),138)

#Successfully extracted the DMN graph
DMN = data[np.ix_(DMN_node_index, DMN_node_index)]

# DMN[abs(DMN) > 0.6] = 1
# DMN[abs(DMN) < 0.6] = 0

#print(DMN[0])

G = nx.from_numpy_matrix(DMN)
G = nx.from_numpy_matrix(data)
# nx.draw(G)
# plt.savefig('example_graph_60.png')
# plt.show()

# Gets rid of all self connections. (ie: Node 0 is connected to Node 0 with a weight of 1)
for i in range(len(DMN_node_index)):
    G.remove_edge(i,i)

def print_graph(G):
    for (u, v, wt) in G.edges.data('weight'):
        print('(%d, %d, %.3f)' % (u, v, wt))

def threshold_with_abs(G, t):
    matrix = nx.to_numpy_matrix(G)
    #print(matrix[0,0])
    for row in range(matrix.shape[0]):
        for col in range(matrix.shape[1]):
            if (abs(matrix[row, col]) > t):
                matrix[row, col] = 1
            else:
                matrix[row, col] = 0
    return nx.from_numpy_matrix(matrix)

def threshold(G, t):
    matrix = nx.to_numpy_matrix(G)
    #print(matrix[0,0])
    for row in range(matrix.shape[0]):
        for col in range(matrix.shape[1]):
            if (matrix[row, col] > t):
                matrix[row, col] = 1
            else:
                matrix[row, col] = 0
    return nx.from_numpy_matrix(matrix)

def sparsity(G):
    num_nodes = nx.number_of_nodes(G)
    num_edges = nx.number_of_edges(G)
    max_edges = (num_nodes * (num_nodes - 1))/2
    return num_edges / max_edges

def find_cutoff(G, sparsity_target, tol):
    left = -1
    right = 1
    left_val = sparsity(threshold(G, left))
    right_val = sparsity(threshold(G, right))
    if left_val < sparsity_target:
        print('Left cutoff must be decreased')
    elif right_val > sparsity_target:
        print('Right cutoff must be decreased')
    else:
        current = (left + right)/2
        curr_val = sparsity(threshold(G, current))
        while abs(curr_val - sparsity_target) > tol:
            if curr_val < sparsity_target:
                right = current
                right_val = sparsity(threshold(G, right))
            else: #curr_val > sparsity_target
                left = current
                left_val = sparsity(threshold(G, left))
            current = (left + right)/2
            curr_val = sparsity(threshold(G, current))
        print('%.3f produces a sparsity of %.3f' % (current, curr_val))
        return current

end = find_cutoff(G, 0.1, tol=1e-3)
start = find_cutoff(G, 0.5, tol=1e-3)
step = 1e-2

threshold_values = []
sparsity_values = []
clustering_co = []
char_path_length = []
avg_short_length = []
global_eff = []
local_eff = []
for t in np.arange(start, end, step):
    threshold_values.append(t)
    #print("Calculating sparsity for %f threshold" % (i))
    new_G = threshold(G, t)
    s = sparsity(new_G)
    sparsity_values.append(s)
    clustering_co.append(nx.average_clustering(new_G))
    global_efficiency = nx.global_efficiency(new_G)
    char_path_length.append(1/global_efficiency)
    global_eff.append(global_efficiency)
    avg_short_length.append(nx.average_shortest_path_length(new_G))
    # local_eff.append(nx.local_efficiency(new_G))
    # print('Done with %.3f' % (t))
    if s == 0:
        break

plt.figure(1, figsize=(9, 6))
plt.subplot(221)
plt.plot(threshold_values, sparsity_values)
plt.xlabel('Threshold')
plt.ylabel('Sparsity')

plt.subplot(222)
plt.plot(sparsity_values, clustering_co)
plt.xlabel('Sparsity')
plt.ylabel('Average Clustering Coefficient')

plt.subplot(223)
plt.plot(sparsity_values, global_eff)
plt.xlabel('Sparsity')
plt.ylabel('Global Efficiency')

plt.subplot(224)
# plt.plot(sparsity_values, local_eff)
plt.plot(sparsity_values, avg_short_length)
plt.xlabel('Sparsity')
# plt.ylabel('Local Efficiency')
plt.ylabel('Avg Short Path Length')

plt.show()


# FILEPATH FOR BRAIN SCANS
# /G-RAID Studio/Raw/Study_Subject_Data/fMRI_structural/Study_pool/NKI-RS/NKI-Neurofeedback
# /DATA/IMAGING DATA-NKI-RS-Neurofeedback/COINS downloaded zip files/nki 6/dicom/triotim
# /ccraddock/neurofeebac_485178/A00059346/444865419_V3/DMN_TRACKING_TEST_0010/DMNTRACKINGTEST.nii.gz
