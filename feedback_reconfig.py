import numpy as np
import pandas as pd
import glob
import os
import csv

################################################################################
# ONLY WORKS WITH READING FILES FROM EXTERNAL HARD DRIVE
################################################################################

# Gets all the files with the fMRI realtime neurofeedback data
files1 = glob.glob('/Volumes/My Passport/DATA/A*/*/*_FEEDBACK_log.txt')
files2 = glob.glob('/Volumes/My Passport/DATA/A*/*/*_FEEDBACK_task_log.txt')
files3 = glob.glob('/Volumes/My Passport/DATA/A*/*/*_NFB_FEEDBACK_[0-9]*')

files = files1 + files2 + files3

def getFeedbackData(fp):
    f = open(fp, 'r')
    lines = f.readlines()
    f.close()

    root = fp[:36]
    print('Writing to:\t' + root + 'neurofeedback.csv')
    f = open(root+'neurofeedback.csv','w')
    file = csv.writer(f,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
    columns = ['Time1','TR','Time2','Left Text','Right Text','Stim Text','Show','Sign','Classifier Output','Cumulative Score']
    file.writerow(columns)
    for i in range(len(lines)):
        if(i >= 11 and i < len(lines)-1):
            lst1 = lines[i].replace(' ','').replace('\n','').split(';')
            lst2 = lines[i+1].replace(' ','').replace('\n','').split(';')
            if(len(lst1) == 3 and len(lst2) == 10):
                columns[0] = lst1[0]
                columns[1] = lst1[2]
                columns[2] = lst2[0]
                columns[3] = lst2[2]
                columns[4] = lst2[3]
                columns[5] = lst2[4]
                columns[6] = lst2[5]
                columns[7] = lst2[6]
                columns[8] = lst2[7]
                columns[9] = lst2[9]
                file.writerow(columns)
                # print(columns)
                # break
    f.close()

for f in files:
    getFeedbackData(f)
