import numpy as np
import pandas as pd
import glob
import os
import csv

################################################################################
# ONLY WORKS WITH READING FILES FROM EXTERNAL HARD DRIVE
################################################################################

files = glob.glob('/Volumes/My Passport/DATA/A*/*/despike_*.txt')

def getElectroData(fp):
    f = open(fp, 'r')
    lines = f.readlines()
    f.close()
    root = fp[:36]
    print("Writing to:\t" + root + 'electro.csv')
    f = open(root+'electro.csv','w')
    file = csv.writer(f,delimiter=',',quotechar='"',quoting=csv.QUOTE_MINIMAL)
    columns = ['','','','']
    for i in range(len(lines)):
        if(i == 10):
            lst = lines[i].replace('\n','').split('\t')
            for i in range(4):
                columns[i] = lst[i]
            # print(columns)
            file.writerow(columns)
        if(i >= 12):
            lst = lines[i].replace('\n','').split('\t')
            for i in range(4):
                columns[i] = lst[i]
            # print(columns)
            # break
            file.writerow(columns)
    f.close()

for f in files:
    getElectroData(f)
