import numpy as np
import numpy.linalg as npl
import pandas as pd
import glob
import networkx as nx
from scipy import stats
import matplotlib.pyplot as plt

from scipy.stats import t as t_dist

def t_stat(y, X, c):
    """ betas, t statistic and significance test given data, design matrix, contrast

    This is OLS estimation; we assume the errors to have independent
    and identical normal distributions around zero for each $i$ in
    $\e_i$ (i.i.d).
    """
    # Make sure y, X, c are all arrays
    y = np.asarray(y)
    X = np.asarray(X)
    c = np.atleast_2d(c).T  # As column vector
    # Calculate the parameters - b hat
    beta = npl.pinv(X).dot(y)
    # The fitted values - y hat
    fitted = X.dot(beta)
    # Residual error
    errors = y - fitted
    # Residual sum of squares
    RSS = (errors**2).sum(axis=0)
    # Degrees of freedom is the number of observations n minus the number
    # of independent regressors we have used.  If all the regressor
    # columns in X are independent then the (matrix rank of X) == p
    # (where p the number of columns in X). If there is one column that
    # can be expressed as a linear sum of the other columns then
    # (matrix rank of X) will be p - 1 - and so on.
    df = X.shape[0] - npl.matrix_rank(X)
    # Mean residual sum of squares
    MRSS = RSS / df
    # calculate bottom half of t statistic
    SE = np.sqrt(MRSS * c.T.dot(npl.pinv(X.T.dot(X)).dot(c)))
    t = c.T.dot(beta) / SE
    # Get p value for t value using cumulative density dunction
    # (CDF) of t distribution
    ltp = t_dist.cdf(t, df) # lower tail p
    p = 1 - ltp # upper tail p
    return beta, t, df, p

# test_matrix = np.zeros((9,30,4))
# print(test_matrix.shape)
#
# new = np.append(test_matrix, np.zeros((9,30,4)), axis=1)
# print(new.shape)

# data = np.zeros((9,2000,3))
#
# # DMN has 58 nodes
# num_nodes = 58
# sparsity = [0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5]
# for s in range(len(sparsity)):
#     num_edges = round(sparsity[s]*(num_nodes)*(num_nodes-1)/2)
#     for i in range(2000):
#         G = nx.gnm_random_graph(num_nodes, num_edges)
#         data[s,i,0] = nx.average_clustering(G)
#         try:
#             avg_short_path = nx.average_shortest_path_length(G)
#             #print('Graph is connected.')
#         except nx.NetworkXError as err:
#             #print(str(err) + ' Setting Avg Shortest Path Len = 0')
#             avg_short_path = 0
#         data[s,i,1] = avg_short_path
#         data[s,i,2] = nx.global_efficiency(G)
#     print('Done with sparsity %.2f' % (sparsity[s]))
#
# np.save('random_graph_measures', data)
# print(data.mean(axis=1))

# num_nodes = 58
# spar = 0.5
# num_edges = round(spar*(num_nodes)*(num_nodes-1)/2)
# G = nx.gnm_random_graph(num_nodes, num_edges)
# nx.draw_spring(G)
# plt.show()

# data = np.load('random_graph_measures.npy')
# sparsity = [0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5]
# for i in range(9):
#     print('Values for sparsity %.3f' % (sparsity[i]))
#     print('\tRandom Clustering Coeff:\t%.5f' % (data[i,:,0].mean()))
#     print('\tMean Random Shortest Path:\t%.5f' % (data[i,:,1].mean()))


#
# subj = glob.glob('/Users/Chris/Desktop/DATA/A*')
# for i in range(len(subj)):
#     subj[i] = subj[i].split('/')[-1]
#
# meditation_data = pd.read_csv('QME.csv',skiprows=1, usecols=[0,6]).values
# valid_meditation = []
# for i in range(meditation_data.shape[0]):
#     if meditation_data[i,0] in subj:
#         valid_meditation.append(meditation_data[i,:].tolist())
#
# print(len(valid_meditation))


# f = np.load('avg_focus_values.npy')
# w = np.load('avg_wander_values.npy')
#
# sparsity = [0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5]
# features = ['Avg Clustering','Global Efficiency','Char Path Len','Avg Short Path Len']
# for i in range(len(sparsity)):
#     print('Calculating sparsity %.2f...' % (sparsity[i]))
#     for j in range(len(features)):
#         res1 = stats.normaltest(f[:,i,j])[1]
#         res2 = stats.normaltest(w[:,i,j])[1]
#         print('\tFocus %s Normality:\t%.5f' % (features[j], res1))
#         print('\tWander %s Normality:\t%.5f' % (features[j], res2))

        # res = stats.ttest_ind(f[:,i,j], w[:,i,j])[1]
        # print('\tPaired t-test p-value for %s:\t%.5f' % (features[j], res))

# print(stats.skew(w[:,4,4]))
# print(stats.skew(np.sqrt(w[:,4,4])))
# print(stats.skew(np.log(w[:,4,4])))
# print(stats.skew(np.reciprocal(-w[:,4,4])))
# print(stats.skew(np.reciprocal(-np.power(w[:,4,4],2))))
# print(stats.skew(np.reciprocal(-np.power(w[:,4,4],3))))
#
# print(np.reciprocal(-np.power(w[:,4,4],3)))

# data = w[:,4,4]
# skewness = stats.skew(data)
# print('Original skewness %.3f' % (skewness))
# val = 0
#
# for i in np.arange(-5,10,0.01):
#     if (stats.skew(stats.boxcox(data,lmbda=i)) < skewness):
#         skewness = stats.skew(stats.boxcox(data,lmbda=i))
#         val = i
#         print('found smaller skewness %.3f at val %d' % (skewness, val))
#
# print(stats.boxcox(data,lmbda=-5))

# for i in range(9):
#     X = np.column_stack((np.ones(w.shape[0]), w[:,i,0], w[:,i,1], w[:,i,2], w[:,i,3]))
#     y = np.reciprocal(np.power(w[:,4,4],2))
#     B = npl.pinv(X).dot(y)
#     print(B)
#     fitted = X.dot(B)
#     errors = y - fitted
#     print(np.sum(errors ** 2))

    # Y = np.asarray(y)
    # B, t, df, p = t_stat(Y, X, [0,0,1,0,-1])
    # print(t)
    # print(p)

all_fv = np.load('all_focus_values.npy')
all_fv2 = np.load('all_focus_values_v2.npy')
# Dim: (9,n,5)
# 9 -> sparsity values
# n -> subj * valid_tr
# 5 -> 4 graph measures (clust, glob_eff, char_path, short_path), meditation value

avg_fv = np.load('avg_focus_values.npy')
avg_fv2 = np.load('avg_focus_values_v2.npy')
# Dim: (n, 9, 5)

all_wv = np.load('all_wander_values.npy')
all_wv2 = np.load('all_wander_values_v2.npy')

avg_wv = np.load('avg_wander_values.npy')
avg_wv2 = np.load('avg_wander_values_v2.npy')

rgm = np.load('random_graph_measures.npy')

for i in range(9): #9 different sparsities
    print(stats.mannwhitneyu(all_fv2[i,:2428,0], all_wv2[i,:,0]))
    print(stats.mannwhitneyu(all_fv2[i,:2428,1], all_wv2[i,:,1]))
    print(stats.mannwhitneyu(all_fv2[i,:2428,2], all_wv2[i,:,2]))
    print(stats.mannwhitneyu(all_fv2[i,:2428,3], all_wv2[i,:,3]))
    print()#should be no meditation value, but all 1s

# for i in range(9):
#     print(stats.mannwhitneyu(all_fv[i,:,0], all_wv[i,:,0]))
#     print(stats.mannwhitneyu(all_fv[i,:,1], all_wv[i,:,1]))
#     print(stats.mannwhitneyu(all_fv[i,:,2], all_wv[i,:,2]))
#     print(stats.mannwhitneyu(all_fv[i,:,3], all_wv[i,:,3]))
#     print()

# for i in range(9):
#     print(stats.wilcoxon(avg_fv[:,i,0], avg_wv[:,i,0]))
#     print(stats.wilcoxon(avg_fv[:,i,1], avg_wv[:,i,1]))
#     print(stats.wilcoxon(avg_fv[:,i,2], avg_wv[:,i,2]))
#     print(stats.wilcoxon(avg_fv[:,i,3], avg_wv[:,i,3]))
#     print()

# gamma_wander = np.zeros((9))
# lmbda_wander = np.zeros((9))
# eff_wander = np.zeros((9))
#
# gamma_focus = np.zeros((9))
# lmbda_focus = np.zeros((9))
# eff_focus = np.zeros((9))
#
# for i in range(9):
#     gamma_wander[i] = avg_wv2[:,i,0].mean()/rgm[i,:,0].mean()
#     lmbda_wander[i] = avg_wv2[:,i,3].mean()/rgm[i,:,1].mean()
#     eff_wander[i] = avg_wv2[:,i,1].mean()/rgm[i,:,2].mean()
#
#     gamma_focus[i] = avg_fv2[:,i,0].mean()/rgm[i,:,0].mean()
#     lmbda_focus[i] = avg_fv2[:,i,3].mean()/rgm[i,:,1].mean()
#     eff_focus[i] = avg_fv2[:,i,1].mean()/rgm[i,:,2].mean()
#
# spar = [0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5]
#
# plt.plot(spar, gamma_focus, label=r'$\gamma = \frac{C^{real}_s}{C^{rand}_s}$')
# plt.plot(spar, lmbda_focus, label=r'$\lambda = \frac{L^{real}_s}{L^{rand}_s}$')
# plt.plot(spar, eff_focus, label=r'$\epsilon = \frac{E^{real}_{glob}}{E^{rand}_{glob}}$')
# plt.xlabel('Sparsity (s = # edges/max edges)')
# plt.title('Small-Worldness of Focus DMN')
# plt.legend()
# plt.show()
